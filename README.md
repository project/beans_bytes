CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Beans & Bytes Theme is compatible with Drupal 9,10.It's a Mobile-friendly Responsive theme.This theme 
features a custom slider, Responsive layout and Highly Customizable.It also 
support Google fonts,font-awesome,and it is very good for any kind of Drupal 
website.Themes allow you to change the look and feel of your Drupal site.

Beans & Bytes Theme is developed with all latest technologies Drupal 9,10
Stable, Font Awesome etc.

 
 * For a full description of the theme, visit the project page:
   https://www.drupal.org/project/beans_bytes

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/beans_bytes

FEATURES
--------

 * Responsive, Mobile-Friendly Theme
 * In built Font Awesome
 * Mobile support (Smartphone, Tablet, Android, iPhone, etc)
 * A total of 10 block regions
 * Custom slider with Custom banner images,title and description.
 * Awesome Drop Down main menus with toggle menu at mobile.

REQUIREMENTS
------------

This theme requires no modules outside of Drupal core. 


INSTALLATION
------------

 * Install the Beans & Bytes theme as you would normally install a
   contributed Drupal theme. Visit
   https://www.drupal.org/docs/extending-drupal/installing-themes for
   further information.


CONFIGURATION
-------------

Navigate to Administration > Appearance and enable the theme.

Available options in the theme settings:

 * Change slider content.
 * Change slider image.
 * Change banner content.
 * Change banner image.
 * Change footer-contact details.
 * Change footer-logo image.
 * Hide/show and change copyright text.
 * Hide/show and change social icons in the footer.
 
 
 
MAINTAINERS
-----------

 * Prachi Jain (prachi-jain) - https://www.drupal.org/u/prachi6824
